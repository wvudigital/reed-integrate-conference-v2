Reed Integrate Conference
==================

**Theme Name:** Theme for Reed Integrate Conference

**Theme Description:** Theme for Super Theme v2.

**Developers name(s):** Austin Rupp

**Bitbucket repository URL:** [https://bitbucket.org/wvudigital/reed-integrate-conference-v2/src](https://bitbucket.org/wvudigital/reed-integrate-conference-v2/src)

**Dependencies necessary to work with this theme:** Sass.



## Gulp & Super Theme

**Requirements**
* [NodeJS](https://nodejs.org)

You will need to install Node ^8.9.4.

  1. Download and install NodeJS from https://nodejs.org/en if you haven't already.
  1. Install Gulp globally by entering `npm install -g gulp` in your terminal.
  1. Navigate to your project's directory via terminal (something like `cd ~/Sites/cleanslate_themes/MY-SITE`)
  1. Install node modules by typing `npm install`
  1. Run Gulp by typing `gulp`.

**Note:** the `gulpfile.js` in its base form will only compile your Sass.
